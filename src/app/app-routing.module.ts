import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../app/pages/home/home.component';
import { PageNotFoundComponent } from '../app/page-not-found/page-not-found.component';
import { LoginComponent } from './auth/login/login.component';

const routes: Routes = [
{ path:'home', component: HomeComponent},
{ path:'auth/login', component: LoginComponent},
{ path: 'layouts',
      loadChildren: () => import('./layouts/layouts.module').then(m => m.LayoutsModule) },
{ path: 'auth',
      loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
{ path: '',
      redirectTo: '/auth/login',
      pathMatch: 'full'
},
{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
