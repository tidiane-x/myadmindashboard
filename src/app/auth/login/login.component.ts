import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import {  Validators,FormBuilder, FormGroup } from '@angular/forms';
//import { User } from './User';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private fb: FormBuilder) { }
  
  ngOnInit() {
    this.initForm();
  }
  initForm(){
    this.loginForm = this.fb.group({   
      password: ['azerty',[Validators.required,Validators.minLength(2),Validators.pattern(/[0-9a-z-A-Z]/)]],//comme ca le password va contenir au moins 2 caracteres
      email:['',[Validators.required,Validators.email,Validators.minLength(2)]]
    });
  }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log('reactiveForm' , this.loginForm.value);
  }
}
