import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavComponent } from '../layouts/nav/nav.component';
import { FooterComponent } from '../layouts/footer/footer.component';
import { SideBarComponent } from '../layouts/side-bar/side-bar.component';
import { ModalComponent } from './modal/modal.component';
const routes: Routes = [
  { path: 'nav', component: NavComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'sideBar', component: SideBarComponent },
  { path: 'modal', component: ModalComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutsRoutingModule { }
