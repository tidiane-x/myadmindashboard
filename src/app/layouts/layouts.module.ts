import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutsRoutingModule } from './layouts-routing.module';
import { NavComponent } from '../layouts/nav/nav.component';
import { FooterComponent } from '../layouts/footer/footer.component';
import { SideBarComponent } from '../layouts/side-bar/side-bar.component';
import { ModalComponent } from './modal/modal.component';

@NgModule({
  declarations: [ NavComponent, FooterComponent, SideBarComponent, ModalComponent ],
  imports: [
    CommonModule,
    LayoutsRoutingModule
  ]
})
export class LayoutsModule { }
